﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wena.Presentation.Models
{
    public class ProfilePageViewModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public string Title { get; set; }

        public string Intro { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        public string DateofBirth { get; set; }
        public string Telephone { get; set; }
        public string HighestQualification { get; set; }
        public string ModeOfIdentity { get; set; }
        public string IdentificationNumber { get; set; }
        public string CountryOfResidence { get; set; }
        public string StateOfResidence { get; set; }
        public string CityOfResidence { get; set; }
        public bool HasRegistered { get; set; }
    }
}