﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wena.Presentation.Models
{
    public class BaseDocument
    {
        public int Id { get; set; }
    }
    public class BlogPost : BaseDocument
    {
        // Will hold HTML from rich text editor
        public string Body { get; set; }
    }
}