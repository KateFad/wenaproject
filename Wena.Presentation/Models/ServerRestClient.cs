﻿using IdentityModel.Client;

using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using static Wena.Presentation.Controller.PersonProfileController;

namespace Wena.Presentation.Models
{
    public class ServerRestClient
    {
        

        private static string Identity = HttpContext.Current.User.Identity.Name;
        Uri _mainonlineurl = new Uri("http://localhost:49216/");
        private RestClient _EdgeClient;
        public ServerRestClient()
        {
            _EdgeClient = new RestClient { BaseUrl = _mainonlineurl };
            }
        public ProfilePageViewModel GetProfileByIdentity()
        {


           var request = new RestRequest
        ("api/Profile/GetProfileByIdentity", Method.GET)
            { RequestFormat = DataFormat.Json };
            request.AddQueryParameter("id", Identity);
            var  response = _EdgeClient.Execute(request);
            try
            {
                var model = Newtonsoft.Json.JsonConvert.DeserializeObject<ProfilePageViewModel>(response.Content.ToString());
                return model;
            }
            catch(Exception e)
            {
                return new ProfilePageViewModel();
            }
            //CustomRenderModel
        }
        public ProfilePageViewModel UpdateProfileByIdentity(ProfilePageViewModel data)
        {


            var request = new RestRequest
         ("api/Profile?id={id}", Method.PUT)
            { RequestFormat = DataFormat.Json };
            request.AddParameter("id", data.Id, ParameterType.UrlSegment);
            request.AddParameter("value", data);
            //request.AddQueryParameter("id", data.Id);
            request.AddBody(data);
            var response = _EdgeClient.Execute<ProfilePageViewModel>(request);
            try
            {
                var model = Newtonsoft.Json.JsonConvert.DeserializeObject<ProfilePageViewModel>(response.Content.ToString());
                return model;
            }
            catch (Exception e)
            {
                return new ProfilePageViewModel();
            }

        }
    }
}