﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Templates;
using Wena.Presentation.Models;

namespace Wena.Presentation.HelperClasses
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString RichTextFor<T>(this HtmlHelper<T> helper, Func<T, string> property) where T : BaseDocument
        {
            var model = helper.ViewData.Model;
            var richText = property(model);
            var withMacros = umbraco.library.RenderMacroContent(richText, model.Id);
            var withLinks = TemplateUtilities.ResolveUrlsFromTextString(withMacros);
            return MvcHtmlString.Create(withLinks);
        }
    }
}