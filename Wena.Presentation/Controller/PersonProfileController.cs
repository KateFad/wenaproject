﻿using Microsoft.IdentityModel.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web;
using Wena.Presentation.Models;
using System.Net.Http;

namespace Wena.Presentation.Controller
{
    public class PersonProfileController : RenderMvcController
    {
        // GET: PersonalPage
        private static ServerRestClient _client = new ServerRestClient();
       
        public override ActionResult Index(RenderModel model)
        {

            //this line means continue and perform the normal rendering
            return base.Index(new CustomRenderModel());
        }
        public class CustomRenderModel : RenderModel
        {
            public ProfilePageViewModel ProfilePageData;
            
            public CustomRenderModel()
                : base(UmbracoContext.Current.PublishedContentRequest.PublishedContent)
            {
                
               
                var profile = _client.GetProfileByIdentity();
             

                ProfilePageData = profile;

            }

           
        }

    }
}