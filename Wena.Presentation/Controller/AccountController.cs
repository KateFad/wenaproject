﻿using IdentityModel.Client;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using Umbraco.Web.Mvc;
using Wena.Presentation.Models;

namespace Wena.Presentation.Controller
{
    //  [Authorize]
    public class AccountController : SurfaceController
    {
        private readonly RestClient _client;
        public static Uri _url = new Uri("http://localhost:5000/connect/token");
        private static ServerRestClient _serverRestClient = new ServerRestClient();

        public AccountController()
        {
            _client = new RestClient { BaseUrl = _url };
        }
        // GET: Account

        [HttpGet]
        [ActionName("MemberLogin")]
        public ActionResult Index()
        {
            return PartialView("LoginForm", new LoginViewModel());
        }

        [HttpGet]
        [ActionName("MemberRegistration")]
        public ActionResult Register()
        {
            return PartialView("LoginForm", new LoginViewModel());
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return Redirect("/");
        }
        private string GetAuthenticationToken(string username, string password)
        {
            //_client = new RestClient { BaseUrl = "http:localhost:500" };
            RestRequest request = new RestRequest() { Method = Method.POST };

            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "password");
            request.AddParameter("client_id", "ro.client");
            request.AddParameter("client_secret", "secret");
            request.AddParameter("scope", "api.sample");
            request.AddParameter("password", "Password.1");
            request.AddParameter("username", "alice");

            var response = _client.Execute(request);


            return response.Content;
        }

        [HttpPost]
        [ActionName("MemberLogin")]
        public async Task<ActionResult> Validate(LoginViewModel model)
        {
            RestRequest request = new RestRequest() { Method = Method.POST };

            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "password");
            request.AddParameter("client_id", "ro.client");
            request.AddParameter("client_secret", "secret");
            request.AddParameter("scope", "api.sample");
            request.AddParameter("password", "Password.1");
            request.AddParameter("username", "alice");

            var response = _client.Execute(request);
            string responseString = response.Content.ToString();
            AccessTokenClass result = JsonConvert.DeserializeObject<AccessTokenClass>(responseString);

            if (result.access_token == null)
            {
                TempData["Status"] = "Invalid Log-in Credentials";
                return RedirectToCurrentUmbracoPage();
            }
            Session["identity"] = model.Email;
            Response.Cookies["UserSettings"]["identity"] = model.Email;
            Response.Cookies["UserSettings"]["apptoken"] = result.access_token;
            Response.Cookies["UserSettings"]["refreshtoken"] = result.refresh_token;
            // call api
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", result.access_token);

            var apiresponse = await httpClient.GetAsync("http://localhost:49216/api/Profile/GetProfileByIdentity");


            bool apiresponsecode = apiresponse.IsSuccessStatusCode;
            if (!apiresponsecode)
            {
                TempData["Status"] = "Invalid Log-in Credentials";
                return RedirectToCurrentUmbracoPage();
            }
            else
            {

                var content = await apiresponse.Content.ReadAsStringAsync();
                TempData["Status"] = true;

                FormsAuthentication.SetAuthCookie(model.Email, model.RememberMe == "1" ? true : false);
                return Redirect("/personal-profile-page/");
                return RedirectToCurrentUmbracoPage();
            }



        }
        [HttpPost]
        [ActionName("MemberRegistration")]
        public async Task<ActionResult> SubmitRegisterForm(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" , ", ModelState.Values
      .SelectMany(v => v.Errors)
      .Select(e => e.ErrorMessage));
                TempData["Status"] = message;
                return RedirectToCurrentUmbracoPage();
            }
            
            Uri _mainonlineurl = new Uri("http://localhost:5000/");
            var _EdgeClient = new RestClient { BaseUrl = _mainonlineurl };

            var request = new RestRequest
             ("api/UserModel/CreateUser", Method.POST)
            { RequestFormat = DataFormat.Json };
            
            request.AddQueryParameter("Email",model.Email);
            request.AddQueryParameter("Category", model.Category);
            request.AddQueryParameter("Password", model.Password);
            string responsemessage = "";
            var response = _EdgeClient.Execute(request);
            responsemessage = response.Content.ToString();
            if(responsemessage.Contains("Success"))
            {
                _mainonlineurl = new Uri("http://localhost:49216/");
                _EdgeClient = new RestClient { BaseUrl = _mainonlineurl };

                 request = new RestRequest
             ("api/Profile", Method.POST)
                { RequestFormat = DataFormat.Json };
                request.AddQueryParameter("Email", model.Email);
                response = _EdgeClient.Execute(request);

                var httpclient = new HttpClient();
                //var httpclientresponse = httpclient.PostAsync("http://localhost:49216/api/Profile", model).Result;
                Response.Cookies["UserSettings"]["identity"] = model.Email;
                FormsAuthentication.SetAuthCookie(model.Email, true);
                return Json(new { Message = "Success", status = "success", Success = true }, JsonRequestBehavior.AllowGet);

                return Redirect("/personal-profile-page/");
            }
            else
            {
                //TempData["Status"] = responsemessage;
                return Json(new { Message = responsemessage + " error", status = "error", Success = false }, JsonRequestBehavior.AllowGet);

                
            }
           
            
           

        }

        [HttpPost]
        [ActionName("UpdateProfile")]
        public async Task<ActionResult> UpdateProfile(ProfilePageViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var message = string.Join(" , ", ModelState.Values
      .SelectMany(v => v.Errors)
      .Select(e => e.ErrorMessage));
                TempData["Status"] = message;
                return RedirectToCurrentUmbracoPage();
            }
            _serverRestClient.UpdateProfileByIdentity(model);
           

            return Redirect("/business-profile-page/");
        }
            

    }
    public class AccessTokenClass
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }

        public string Scope { get; set; }
        public string refresh_token { get; set; }

        public string xoauth_yahoo_guid { get; set; }
    }
    public class UserModel
    {

        public string SubjectId { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }




    }
}