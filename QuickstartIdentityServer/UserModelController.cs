﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.MongoDB;
using QuickstartIdentityServer.Quickstart.Interface;
using System.Security.Claims;
using IdentityModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace QuickstartIdentityServer
{
    [Route("api/[controller]")]
    public class UserModelController : ControllerBase
    {
        protected readonly IRepository _dbRepository;
        private readonly IPasswordHasher<IdentityUser> _passwordHasher;
        private readonly UserManager<IdentityUser> _usermanager;
        public UserModelController(UserManager<IdentityUser> usermanager, IRepository repository, IPasswordHasher<IdentityUser> passwordHasher)
        {
            _dbRepository = repository;
            _passwordHasher = passwordHasher;
            _usermanager = usermanager;

        }
        // GET: api/values
        //[HttpGet]
        //[Route("GetValue")]
        //public IEnumerable<string> GetValue()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //[Route("GetUser")]
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}
  
        [Route("CreateUser")]
        [HttpPost]
        // POST api/values
        public async Task<string> CreateUser(UserModel modelClass)
        {
           
            modelClass.SubjectId = _usermanager.Users.Count().ToString();
            var Claims = new List<Claim>
                    {
                       
                        new Claim(JwtClaimTypes.Email, modelClass.Email),
                        new Claim(JwtClaimTypes.WebSite, "https://"+ modelClass.Email + ".com")
                    };

            var user = new IdentityUser()
            {
                UserName = modelClass.Email,
                LockoutEnabled = false,
                EmailConfirmed = true,
                Email = modelClass.Email,
                NormalizedEmail = modelClass.Email
            };

            foreach (var claim in Claims)
            {
                user.AddClaim(claim);
            }
            
            var result = _usermanager.CreateAsync(user, modelClass.Password);
            if (!result.Result.Succeeded)
            {
                var message = string.Join("  ", result.Result.Errors.Select(v => v.Description));
                var errors = result.Result.Errors;
                
                return message;// "Email has been taken";
            }
            return  "Success";
        }

        [Route("ForgotCredentials")]
        [HttpPost]
        // POST api/values
        public string UpdateUserCredentials(UpdadeUserModelCredentials modelClass)
        {
            

            var user =  _usermanager.FindByEmailAsync( modelClass.Username);
            if(user.Result != null)
            {
                var result = _usermanager.ChangePasswordAsync(user.Result, modelClass.OldPassword, modelClass.Password);
                if (!result.Result.Succeeded)
                {
                    return "Invalid";
                }
            }
            else
            {
                return "Invalid";
            }

            
            return "Success";
        }
        [HttpPost]
        [Route("ResetUserCredentialCode")]
        // POST api/values
        public string ResetUserCredentialCode(UpdadeUserModelCredentials modelClass)
        {


            var user = _usermanager.FindByEmailAsync(modelClass.Username);
            if (user.Result != null)
            {
                var result = _usermanager.GeneratePasswordResetTokenAsync(user.Result);
                
                    return result.Result;
                
            }
            else
            {
                return "";
            }


            return "";
        }

        [Route("ResetUserCredentials")]
        [HttpPost]
        // POST api/values
        public string ResetUserCredentials(ResetUserModelCredentials modelClass)
        {


            var user = _usermanager.FindByEmailAsync(modelClass.Username);
            if (user.Result != null)
            {
                var result = _usermanager.ResetPasswordAsync(user.Result,modelClass.Code, modelClass.NewPassword);
                if (!result.Result.Succeeded)
                {
                    return "Invalid";
                }
            }
            else
            {
                return "Invalid";
            }


            return "Success";
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
