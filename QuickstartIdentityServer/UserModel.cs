﻿using IdentityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuickstartIdentityServer
{
    public class UserModel
    {

        public string SubjectId { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
        public string Category { get; set; }



    }
    public class UpdadeUserModelCredentials
    {

        public string SubjectId { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public string OldPassword { get; set; }


    }

    public class ResetUserModelCredentials
    {

        public string Username { get; set; }
        public string Code { get; set; }
        public string NewPassword { get; set; }

        public string OldPassword { get; set; }


    }
}
