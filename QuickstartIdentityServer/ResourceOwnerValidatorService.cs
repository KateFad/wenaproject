﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.MongoDB;
using QuickstartIdentityServer.Quickstart.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace QuickstartIdentityServer
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        protected readonly IRepository _dbRepository;
        private readonly IPasswordHasher<IdentityUser> _passwordHasher;
        private readonly UserManager<IdentityUser> _usermanager;
        public ResourceOwnerPasswordValidator(UserManager<IdentityUser> usermanager, IRepository repository, IPasswordHasher<IdentityUser> passwordHasher)
        {
            _dbRepository = repository;
            _passwordHasher = passwordHasher;
            _usermanager = usermanager;
            
        }

        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var user = await _usermanager.FindByNameAsync(context.UserName);
            if (user != null && await _usermanager.CheckPasswordAsync(user, context.Password))
            {
                context.Result = new GrantValidationResult(
                    subject: "2",
                    authenticationMethod: "custom",
                    claims: new Claim[]
                {
                    //Firstname and Surname are DB columns mapped to User object (from table [User])
                    new Claim(JwtClaimTypes.Name, user.UserName),
                    new Claim(JwtClaimTypes.Email, user.Email),
                    new Claim(JwtClaimTypes.Role, user.Roles.ToString()),
                    //custom claim
                    
                }
                    );


            }
            else
            {
                context.Result = new GrantValidationResult(
                       TokenRequestErrors.InvalidGrant,
                       "invalid custom credential");
            }


            return;

        }
    }
}
