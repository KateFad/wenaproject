﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using IdentityServer4.Stores;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace IdentityServer.App
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(ILoggerFactory loggerFactory, IHostingEnvironment env)
        {
            var environmentVar = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (environmentVar == null)
            {
                environmentVar = env.EnvironmentName;
            }
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environmentVar}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            //--- Configure Serilog ---
            //var serilog = new LoggerConfiguration()
            //                    .ReadFrom.Configuration(Configuration);

            //loggerFactory.WithFilter(new FilterLoggerSettings
            //            {
            //                { "IdentityServer", LogLevel.Error },
            //                { "Microsoft", LogLevel.Error },
            //                { "System", LogLevel.Error },
            //            })
            //            .AddSerilog(serilog.CreateLogger());
        }


        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentityServer(
                               // Enable IdentityServer events for logging capture - Events are not turned on by default
                               options =>
                               {
                                   options.Events.RaiseSuccessEvents = true;
                                   options.Events.RaiseFailureEvents = true;
                                   options.Events.RaiseErrorEvents = true;
                               }
                           )
                           .AddTemporarySigningCredential()
                           .AddMongoRepository()
                           .AddMongoDbForAspIdentity<IdentityUser, IdentityRole>(Configuration)
                           .AddClients()
                           .AddIdentityApiResources()
                           .AddPersistedGrants()
                           //.AddTestUsers(Config.GetUsers())
                           .AddProfileService<ProfileService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(LogLevel.Debug);

           // Log.Information("IdentityServer4.Configure was executed...");

            app.UseDeveloperExceptionPage();

            app.UseIdentityServer();
            app.UseMongoDbForIdentityServer();
           
        }
    }
}
