﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelCore.Models
{
    public class BaseClass
    {
        public DateTime DateCreated { get; private set; }
        public Guid Key { get; private set; }
        public bool IsDelete { get; set; }
        public DateTime DateDeleted { get; set; }
        public DateTime DateModified { get; set; }
        public BaseClass()
        {
            DateCreated = DateTime.UtcNow;
            DateDeleted = DateTime.UtcNow;
            DateModified = DateTime.UtcNow;
            Key = Guid.NewGuid();
            IsDelete = false;

        }
    }
}
