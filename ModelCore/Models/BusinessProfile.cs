﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelCore.Models
{
   public  class BusinessProfile: BaseClass
    {
        public string Id { get; set; }

        public string BusinessConcept { get; set; }
        public string BusinessSector { get; set; }
        public string BusinessStatus { get; set; }
        public string StateOfOperation { get; set; }

        public string CountryOfOperation { get; set; }

        public string OperatingAddressState { get; set; }

        public string OperatingAddressTown { get; set; }

        public string CommencementMonth { get; set; }

        public string CommencementYear { get; set; }

        // 
        public string BusinessCategory { get; set; }

        public Guid ProfileFormId { get; set; }

        public virtual Profile Profile { get; set; }
    }
}
