﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModelCore.Models
{
    public class CompanyDetail: BaseClass
    {
        public string Id { get; set; }

        public string BusinessEntity { get; set; }
        public string BusinessName { get; set; }
        public string RegistrationNumber { get; set; }
        public string RomeInCompany { get; set; }

        public string RegistrationDate { get; set; }

        public string HasBankAccount { get; set; }

        public string BusinessStatus { get; set; }

        public string CountOfPermanentEmployees { get; set; }

        public string CountOfTemporaryEmployees { get; set; }

        public string DetailsOnFinanicialRecordUpkeep { get; set; }

        // 
        public bool HasFinancialRecordOfficer { get; set; }

        public Guid ProfileFormId { get; set; }

        public virtual Profile Profile { get; set; }
    }
}

