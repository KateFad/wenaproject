﻿using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace RegisterService.Test
{
    class Program
    {

        static void Main(string[] args) => MainAsync().GetAwaiter().GetResult();


        public static async Task MainAsync()
        {

            Console.WriteLine("+-----------------------+");
            Console.WriteLine("|  Sign in with OIDC    |");
            Console.WriteLine("+-----------------------+");
            Console.WriteLine("");
            Console.WriteLine("Press any key to sign in...");
            Console.ReadKey();

            await SignIn();
        }

        private static async Task SignIn()

        {
            
            var disco = await DiscoveryClient.GetAsync("http://localhost:5000");

            // request token
            var tokenClient = new TokenClient(disco.TokenEndpoint, "client", "secret");

            var tokenResponse = await tokenClient.RequestClientCredentialsAsync("api.sample");


            var tokenClient1 = new TokenClient(disco.TokenEndpoint, "ro.client", "secret");
            var tokenResponse1 = await tokenClient1.RequestResourceOwnerPasswordAsync("tope@email.com", "Password.1","api.sample");


           // var tokenResponse = await tokenClient.RequestClientCredentialsAsync("api.sample");

            if (tokenResponse1.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            Console.WriteLine(tokenResponse.Json);
            Console.WriteLine("\n\n");

            // call api
            var client = new HttpClient();
            client.SetBearerToken(tokenResponse.AccessToken);

            var response = await client.GetAsync("http://localhost:49216/values");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine("working");
            }

            Console.WriteLine("Press Enter to close app...");
            Console.ReadKey();
            Console.ReadLine();
        }
    }
}

