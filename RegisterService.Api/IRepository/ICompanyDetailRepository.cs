﻿using ModelCore.Models;
using RegisterService.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegisterService.Api.IRepository
{
    public interface ICompanyDetailRepository
    {
        Task<IEnumerable<CompanyDetail>> GetAllCompanyDetails();
        Task<CompanyDetail> GetCompanyDetail(string id);
        Task AddCompanyDetail(CompanyDetail item);
        Task<bool> RemoveCompanyDetail(string id);
        Task<bool> UpdateCompanyDetail(string id, CompanyDetail body);
        Task<bool> RemoveAllCompanyDetails();
    }
}
