﻿using ModelCore.Models;
using RegisterService.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegisterService.Api.IRepository
{
    public interface IBusinessProfileRepository
    {
        Task<IEnumerable<BusinessProfile>> GetBusinessProfiles();
        Task<BusinessProfile> GetBusinessProfile(string id);
        Task AddBusinessProfile(BusinessProfile item);
        Task<bool> RemoveBusinessProfile(string id);

        Task<bool> UpdateBusinessProfile(string id, BusinessProfile body);
        Task<bool> RemoveAllBusinessProfiles();
    }
}
