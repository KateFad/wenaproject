﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegisterService.Api.Repository
{
    public interface IUnitOfWork
    {
       // IRepo<T> RepositoryFor<T>() where T : class;
        void Save();
    }
}
