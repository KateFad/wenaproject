﻿using ModelCore.Models;
using RegisterService.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegisterService.Api.IRepository
{
    public interface IProfileRepository
    {
        Task<IEnumerable<Profile>> GetAllProfiles();
        Task<Profile> GetProfile(string id);

        Task<Profile> GetProfileByIdentiyId(string id);
        Task AddProfile(Profile item);
        Task<bool> RemoveProfile(string id);
        Task<bool> UpdateProfile(string id, Profile body);
        Task<bool> RemoveAllProfiles();
    }
}
