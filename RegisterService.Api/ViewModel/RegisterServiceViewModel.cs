﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegisterService.Api.ViewModel
{
    public class RegisterServiceViewModel
    {
        public class BaseClassViewModel
        {
            public DateTime DateCreated { get; private set; }
            public Guid Key { get; private set; }
            public bool IsDelete { get; set; }
            public DateTime DateDeleted { get; set; }
            public DateTime DateModified { get; set; }

        }

        public class BusinessProfileViewModel : BaseClassViewModel
        {
            public string Id { get; set; }

            public string BusinessConcept { get; set; }
            public string BusinessSector { get; set; }
            public string BusinessStatus { get; set; }
            public string StateOfOperation { get; set; }

            public string CountryOfOperation { get; set; }

            public string OperatingAddressState { get; set; }

            public string OperatingAddressTown { get; set; }

            public string CommencementMonth { get; set; }

            public string CommencementYear { get; set; }

            // 
            public string BusinessCategory { get; set; }

            public Guid ProfileFormId { get; set; }

            public virtual ProfileViewModel Profile { get; set; }
        }

        public class CompanyDetailViewmodel : BaseClassViewModel
        {
            public string Id { get; set; }

            public string BusinessEntity { get; set; }
            public string BusinessName { get; set; }
            public string RegistrationNumber { get; set; }
            public string RomeInCompany { get; set; }

            public string RegistrationDate { get; set; }

            public string HasBankAccount { get; set; }

            public string BusinessStatus { get; set; }

            public string CountOfPermanentEmployees { get; set; }

            public string CountOfTemporaryEmployees { get; set; }

            public string DetailsOnFinanicialRecordUpkeep { get; set; }

            // 
            public bool HasFinancialRecordOfficer { get; set; }

            public Guid ProfileFormId { get; set; }

            public virtual ProfileViewModel Profile { get; set; }
        }

        public class ProfileViewModel : BaseClassViewModel
        {
            public string Id { get; set; }

            public string IdenityId { get; set; }

            public string Title { get; set; }

            public string Intro { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string MiddleName { get; set; }
            public string Gender { get; set; }
            public string DateofBirth { get; set; }
            public string Telephone { get; set; }
            public string HighestQualification { get; set; }
            public string ModeOfIdentity { get; set; }
            public string IdentificationNumber { get; set; }
            public string CountryOfResidence { get; set; }
            public string StateOfResidence { get; set; }
            public string CityOfResidence { get; set; }
            public bool HasRegistered { get; set; }
        }
    }

}

