﻿using Microsoft.Extensions.Options;
using ModelCore.Models;
using MongoDB.Driver;
using RegisterService.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegisterService.Api
{
    public class RegistrationContext
    {
        private readonly IMongoDatabase _database = null;

        public RegistrationContext(IOptions<MongoDbsettinngs> settings)
        {
            
            var client = new MongoClient(settings.Value.MongoConnection);
            if (client != null)
                _database = client.GetDatabase(settings.Value.MongoDatabaseName);
        }

        public IMongoCollection<Note> Notes
        {
            get
            {
                return _database.GetCollection<Note>("Note");
            }
        }

        public IMongoCollection<CompanyDetail> CompanyDetails
        {
            get
            {
                return _database.GetCollection<CompanyDetail>("CompanyDetail");
            }
        }

        public IMongoCollection<Profile> Profiles
        {
            get
            {
                return _database.GetCollection<Profile>("Profile");
            }
        }
        public IMongoCollection<BusinessProfile> BusinessProfiles
        {
            get
            {
                return _database.GetCollection<BusinessProfile>("BusinessProfile");
            }
        }
    }
}
