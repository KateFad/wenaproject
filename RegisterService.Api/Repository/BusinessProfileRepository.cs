﻿using Microsoft.Extensions.Options;
using ModelCore.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using RegisterService.Api.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegisterService.Api.Repository
{
    public class BusinessProfileRepository:IBusinessProfileRepository
    {
        private readonly RegistrationContext _context = null;

        public BusinessProfileRepository(IOptions<MongoDbsettinngs> settings)
        {
            _context = new RegistrationContext(settings);
        }

        public async Task<IEnumerable<BusinessProfile>> GetBusinessProfiles()
        {
            try
            {
                return await _context.BusinessProfiles
                        .Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<BusinessProfile> GetBusinessProfile(string id)
        {
            var filter = Builders<BusinessProfile>.Filter.Eq("Id", id);

            try
            {
                return await _context.BusinessProfiles
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task AddBusinessProfile(BusinessProfile item)
        {
            try
            {
                await _context.BusinessProfiles.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<bool> RemoveBusinessProfile(string id)
        {
            try
            {
                DeleteResult actionResult = await _context.BusinessProfiles.DeleteOneAsync(
                        Builders<BusinessProfile>.Filter.Eq("Id", id));

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }



        public async Task<bool> UpdateBusinessProfile(string id, BusinessProfile item)
        {
            try
            {
                ReplaceOneResult actionResult
                    = await _context.BusinessProfiles
                                    .ReplaceOneAsync(n => n.Id.Equals(id)
                                            , item
                                            , new UpdateOptions { IsUpsert = true });
                return actionResult.IsAcknowledged
                    && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<bool> RemoveAllBusinessProfiles()
        {
            try
            {
                DeleteResult actionResult
                    = await _context.BusinessProfiles.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }


    }
}