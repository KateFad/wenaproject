﻿using Microsoft.Extensions.Options;
using ModelCore.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using RegisterService.Api.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegisterService.Api.Repository
{
    public class ProfileRepository:IProfileRepository
    {
        private readonly RegistrationContext _context = null;

        public ProfileRepository(IOptions<MongoDbsettinngs> settings)
        {
            _context = new RegistrationContext(settings);
        }

        public async Task<IEnumerable<Profile>> GetAllProfiles()
        {
            try
            {
                return await _context.Profiles
                        .Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<Profile> GetProfile(string id)
        {
            var filter = Builders<Profile>.Filter.Eq("Id", id);

            try
            {
                return await _context.Profiles
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<Profile> GetProfileByIdentiyId(string id)
        {
            var filter = Builders<Profile>.Filter.Eq("Email", id);

            try
            {
                return await _context.Profiles
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task AddProfile(Profile item)
        {
            try
            {
                await _context.Profiles.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<bool> RemoveProfile(string id)
        {
            try
            {
                DeleteResult actionResult = await _context.Profiles.DeleteOneAsync(
                        Builders<Profile>.Filter.Eq("Id", id));

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }



        public async Task<bool> UpdateProfile(string id, Profile item)
        {
            try
            {
                ReplaceOneResult actionResult
                    = await _context.Profiles
                                    .ReplaceOneAsync(n => n.Id.Equals(id)
                                            , item
                                            , new UpdateOptions { IsUpsert = true });
                return actionResult.IsAcknowledged
                    && actionResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<bool> RemoveAllProfiles()
        {
            try
            {
                DeleteResult actionResult
                    = await _context.Profiles.DeleteManyAsync(new BsonDocument());

                return actionResult.IsAcknowledged
                    && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }


    }
}