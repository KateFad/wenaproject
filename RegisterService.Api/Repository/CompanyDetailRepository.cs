﻿using Microsoft.Extensions.Options;
using ModelCore.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using RegisterService.Api.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RegisterService.Api.Repository
{
    public class CompanyDetailRepository:ICompanyDetailRepository
    {
                    private readonly RegistrationContext _context = null;

            public CompanyDetailRepository(IOptions<MongoDbsettinngs> settings)
            {
                _context = new RegistrationContext(settings);
            }

            public async Task<IEnumerable<CompanyDetail>> GetAllCompanyDetails()
            {
                try
                {
                    return await _context.CompanyDetails
                            .Find(_ => true).ToListAsync();
                }
                catch (Exception ex)
                {
                    // log or manage the exception
                    throw ex;
                }
            }

            public async Task<CompanyDetail> GetCompanyDetail(string id)
            {
                var filter = Builders<CompanyDetail>.Filter.Eq("Id", id);

                try
                {
                    return await _context.CompanyDetails
                                    .Find(filter)
                                    .FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    // log or manage the exception
                    throw ex;
                }
            }

            public async Task AddCompanyDetail(CompanyDetail item)
            {
                try
                {
                    await _context.CompanyDetails.InsertOneAsync(item);
                }
                catch (Exception ex)
                {
                    // log or manage the exception
                    throw ex;
                }
            }

            public async Task<bool> RemoveCompanyDetail(string id)
            {
                try
                {
                    DeleteResult actionResult = await _context.CompanyDetails.DeleteOneAsync(
                            Builders<CompanyDetail>.Filter.Eq("Id", id));

                    return actionResult.IsAcknowledged
                        && actionResult.DeletedCount > 0;
                }
                catch (Exception ex)
                {
                    // log or manage the exception
                    throw ex;
                }
            }

          

            public async Task<bool> UpdateCompanyDetail(string id, CompanyDetail item)
            {
                try
                {
                    ReplaceOneResult actionResult
                        = await _context.CompanyDetails
                                        .ReplaceOneAsync(n => n.Id.Equals(id)
                                                , item
                                                , new UpdateOptions { IsUpsert = true });
                    return actionResult.IsAcknowledged
                        && actionResult.ModifiedCount > 0;
                }
                catch (Exception ex)
                {
                    // log or manage the exception
                    throw ex;
                }
            }

            public async Task<bool> RemoveAllCompanyDetails()
            {
                try
                {
                    DeleteResult actionResult
                        = await _context.CompanyDetails.DeleteManyAsync(new BsonDocument());

                    return actionResult.IsAcknowledged
                        && actionResult.DeletedCount > 0;
                }
                catch (Exception ex)
                {
                    // log or manage the exception
                    throw ex;
                }
            }

         
        }
    }