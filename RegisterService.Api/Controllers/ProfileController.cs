﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RegisterService.Api.IRepository;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using ModelCore.Models;
using MongoDB.Bson;
using System.Net.Http;

namespace RegisterService.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Profile")]
   // [Authorize]
    public class ProfileController : ControllerBase
    {

        private readonly IProfileRepository _profileRepository;

        public ProfileController(IProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }

        // GET: notes/notes
        [HttpGet]
        public Task<string> Get()
        {
            return GetProfileInternal();
        }

        [HttpGet]
        private async Task<string> GetProfileInternal()
        {
            var items = await _profileRepository.GetAllProfiles();

            string rawJsonFromDb = JsonConvert.SerializeObject(items, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });


            return rawJsonFromDb;
        }

        [HttpGet]
        [Route("GetProfileById")]
        public async Task<IActionResult> GetProfileById(string id)
        {
            var items = await _profileRepository.GetProfile(id);

            string rawJsonFromDb = JsonConvert.SerializeObject(items, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

           
            return Ok(rawJsonFromDb);
        }


        [HttpGet]
        [Route("GetProfileByIdentity")]
        public async Task<IActionResult> GetProfileByIdentity(string id)
        {
            var items = await _profileRepository.GetProfileByIdentiyId(id);

            string rawJsonFromDb = JsonConvert.SerializeObject(items, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });


            return Ok(items);
        }


        // POST api/notes
        [HttpPost]
        public async Task<IActionResult> Post(Profile value)
        {
            //value.HasRegistered = true;
            value.Id = ObjectId.GenerateNewId().ToString();
            try
            {
               await _profileRepository.AddProfile(value);
                return Ok("true");
            }
            catch(Exception e)
            {
                return Ok(e.InnerException.Message);
            }
        }

        // PUT api/notes/5
        [HttpPut]
        public async Task<IActionResult> Put(string id, Profile value)
        {
            try
            {
                await _profileRepository.UpdateProfile(id, value);
                return Ok("true");
            }
            catch (Exception e)
            {
                return Ok(e.InnerException.Message);
            }
            
        }

        // DELETE api/notes/5
        [HttpDelete]
        public async Task<IActionResult> Delete(string id)
        {
           
            try
            {
                await _profileRepository.RemoveProfile(id);
                return Ok("true");
            }
            catch (Exception e)
            {
                return Ok(e.InnerException.Message);
            }

        }
    }
  
}
