﻿using LearningPlatform.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegisterService.Api.Repository
{
    public class UnitOfWork
    {
        private readonly DbModel  _db = new DbModel();
        public IRepo<T> RepositoryFor<T>() where T : class
        {
            return new Repository<T>(_db);
        }

        public void Save()
        {
            _db.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}